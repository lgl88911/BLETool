# BLETool

### 项目介绍
Tool for BLE debug, base bluez
1. bluez_tool/btgatt-client: modify btgatt-client for show gatt spec uuid name
2. pytool/pyCovert: create gatt sepc uuid name for "C" array


### 使用说明

#### bluez_tool
1. Download & decompress bluez http://www.bluez.org/release-of-bluez-5-50/
2. Replace bluez_tools/ file to bluez/tools
3. enable macro "#define SHOW_NAME" of bluez_tools/btgatt-client.c
3. Build  
4. go into bluez/tools do: ./btgatt-client -t random -d E9:45:EC:29:65:DB

#### pyCovert
1. Get table from gatt spec web. eg: https://www.bluetooth.com/specifications/gatt/characteristics
2. Save table to excel. eg: pytools/pyCovert/GATT Characteristics.xlsx
3. Remove column & only leave Name & uuid， save to csv. eg: pytools/pyCovert/GATT Characteristics.csv
4. covert csv to "C" arrag to out.csv eg: python pytool/pyCovert/pyCovert.py GATT Characteristics.csv
5. add content of out.csv to uuid_map struuidmap[] of bluez_tools/btgatt-client.c
5. you can add custom {name,uuid} to uuid_map struuidmap[], eg {"my home", 0x33468892}

### 参与贡献

1. Fork 本项目
2. 新建 Feat_xxx 分支
3. 提交代码
4. 新建 Pull Request


